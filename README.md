# MavenTarget
This is an empty project, which acts as a centralized Maven remote. This way we
can use the libraries, but only have to provide one Maven repository.
